FROM ubuntu:16.04


ARG METAMOD_VERSION
ARG SOURCEMOD_VERSION
ARG GSLT_TOKEN


RUN apt-get -y update \
    && apt-get -y upgrade \
    && apt-get -y install lib32gcc1 net-tools curl unzip \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV STEAMCMD_DIR /steamcmd
ENV SERVER_DIR $STEAMCMD_DIR/csgoserver

# Install SteamCMD
RUN mkdir -p $STEAMCMD_DIR
RUN curl -fsSL http://media.steampowered.com/client/steamcmd_linux.tar.gz | tar -C $STEAMCMD_DIR -xvz

# Create update script
RUN echo "login anonymous \nforce_install_dir $SERVER_DIR \napp_update 740 validate \nquit" > $STEAMCMD_DIR/csgo_ds.txt

# Download CS:GO server using SteamCMD
RUN $STEAMCMD_DIR/steamcmd.sh +runscript csgo_ds.txt

# Install MetaMod
RUN curl -fsSL http://mms.alliedmods.net/mmsdrop/1.10/mmsource-${METAMOD_VERSION}-linux.tar.gz | tar -C $SERVER_DIR/csgo -xvz


# Install SourceMod
RUN curl -fsSL https://sm.alliedmods.net/smdrop/1.8/sourcemod-${SOURCEMOD_VERSION}-linux.tar.gz | tar -C $SERVER_DIR/csgo -xvz

# Install SourceMod plugins
ENV SOURCEMOD_PLUGINS_DIR $SERVER_DIR/csgo/addons/sourcemod/plugins

# 1. Automatic plugins updater
RUN curl -fsSL https://bitbucket.org/GoD_Tony/updater/downloads/updater.smx -o $SOURCEMOD_PLUGINS_DIR/updater.smx

# 2. Shavit
RUN curl -fsSL https://github.com/shavitush/bhoptimer/releases/download/v2.5.6/bhoptimer-v2.5.6.zip -o bhoptimer.zip \
    && unzip bhoptimer.zip -d $SERVER_DIR/csgo \
    && rm -f bhoptimer.zip

# Add admin(s)
RUN echo '\"STEAM_1:0:36910097\" \"99:z\"' >> ./csgoserver/csgo/addons/sourcemod/configs/admins_simple.ini

# Add custom config files
COPY cfg $SERVER_DIR/csgo/cfg/

# Add sourcemode databases file
COPY sourcemod_cfg/databases.cfg $SERVER_DIR/csgo/addons/sourcemod/configs/

EXPOSE 27015/udp

WORKDIR $SERVER_DIR



USER csgo
# Default entrypoint runs the server with 128 tickrate and autoupdates
ENTRYPOINT ["./srcds_run", "-game", "csgo", "-tickrate", "128", "-autoupdate", "-steam_dir", "..", "-steamcmd_script", "csgo_ds.txt", "+sv_setsteamaccount", ${GSLT_TOKEN}]

# Default cmd adds arguments for competetive mode with de_cache a default map.
# WARNING: this will only work as a LAN server. To make it public you must specify GSLT using +sv_setsteamaccount argument.
# CMD ["-console", "-usercon", "+game_type", "0", "+game_mode", "1", "+mapgroup", "mg_active", "+map", "de_cache"]