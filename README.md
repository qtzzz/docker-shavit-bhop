# Dockerized csgo server using shavit bhop timer

## Docker configuration

- In order to increase the max container size, we need to change the storage driver. Be careful, the following command list will make you change it and delete every docker images/containers/volumes/... on your system
- stop the docker service
- Edit /etc/docker/daemon.json 

        {
            "storage-driver": "devicemapper"
        }
- Clean up /var/lib/docker

        rm -r /var/lib/docker/*        
- Start the daemon with custom option

        dockerd --storage-opt dm.basesize=40G  
- When it says ```Daemon has completed initialization``` you can press ctrl+c
- You can now restart the docker service 

## How to use the project

1. Clone the project
2. Change the database password in both ```docker-compose.yaml``` and ```sourcemod_cfg/databases.cfg```
3. Change the rcon password in cfg/server.cfg
4. Add your gslt token in ```docker-compose.yaml``` 
5. Create a maps directory and add your maps in it 
6. Now run

        docker-compose up -d

